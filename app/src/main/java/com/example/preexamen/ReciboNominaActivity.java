package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina recibo;
    private TextView lblNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtras;
    private RadioButton rdbAuxiliar;
    private RadioButton rdbAlbanil;
    private RadioButton rdbObra;
    private TextView lblSubTotal;
    private TextView lblImpuesto;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);

        lblNombre = findViewById(R.id.txtNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rdbAuxiliar = findViewById(R.id.auxiliar);
        rdbAlbanil = findViewById(R.id.albanil);
        rdbObra = findViewById(R.id.ingObra);
        lblSubTotal = findViewById(R.id.txtSubTotal);
        lblImpuesto = findViewById(R.id.txtImpuesto);
        lblTotal = findViewById(R.id.txtTotalAPagar);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        //Recibe el nombre de usuario
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblNombre.setText("Nombre: " + nombre);

        //genera y muestra el numero de recibo
        recibo = new ReciboNomina();
        recibo.setNumRecibo(recibo.generarFolio());

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarEntradas()) {
                    String nombre = lblNombre.getText().toString();
                    int horasNormales = Integer.parseInt(txtHorasNormal.getText().toString());
                    int horasExtras = Integer.parseInt(txtHorasExtras.getText().toString());
                    int puesto = obtenerPuestoSeleccionado();
                    float porcentajeImpuesto = 16;

                    recibo.setNombre(nombre);
                    recibo.setHorasTrabNormal(horasNormales);
                    recibo.setHorasTrabExtras(horasExtras);
                    recibo.setPuesto(puesto);
                    recibo.setPorcentajeImpuesto(porcentajeImpuesto);

                    float subtotal = recibo.calcularSubtotal();
                    float impuesto = recibo.calcularImpuesto();
                    float total = recibo.calcularTotal();

                    lblSubTotal.setText(getString(R.string.label_subtotal) + ": $" + subtotal);
                    lblImpuesto.setText(getString(R.string.label_impuesto) + ": $" + impuesto);
                    lblTotal.setText(getString(R.string.label_total) + ": $" + total);

                } else {
                    Toast.makeText(ReciboNominaActivity.this, R.string.msg_error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarEntradas() {
        return !txtHorasNormal.getText().toString().isEmpty() &&
                !txtHorasExtras.getText().toString().isEmpty() &&
                (rdbAuxiliar.isChecked() || rdbAlbanil.isChecked() || rdbObra.isChecked());
    }

    private int obtenerPuestoSeleccionado() {
        if (rdbAuxiliar.isChecked()) return 1;
        if (rdbAlbanil.isChecked()) return 2;
        if (rdbObra.isChecked()) return 3;
        return 0;
    }

    private void limpiarCampos() {
        txtHorasNormal.setText("");
        txtHorasExtras.setText("");
        rdbAuxiliar.setChecked(false);
        rdbAlbanil.setChecked(false);
        rdbObra.setChecked(false);
        lblSubTotal.setText(R.string.label_subtotal);
        lblImpuesto.setText(R.string.label_impuesto);
        lblTotal.setText(R.string.label_total);
    }
}
